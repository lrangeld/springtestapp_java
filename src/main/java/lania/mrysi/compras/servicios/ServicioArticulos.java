package lania.mrysi.compras.servicios;

import java.math.BigDecimal;
import lania.mrysi.compras.entidades.Articulo;
import lania.mrysi.compras.repositorios.RepositorioArticulos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author LANIA
 */
@Service
public class ServicioArticulos {
    
    @Autowired
    RepositorioArticulos repoArticulos;
            
    private static final BigDecimal LIMITE = new BigDecimal("1000000");
    
    public void guardarArticulo(Articulo art) {
        // APLICAR VALIDACION
        if (LIMITE.compareTo(art.getPrecio()) < 0) {
            throw new IllegalArgumentException();
        }
        repoArticulos.save(art);
    }
}
