package lania.mrysi.compras.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author ljrangeld
 */
@Entity
@Table(name = "proveedores")
@NamedStoredProcedureQuery(name="Proveeedor.actualizarCantidades",
        procedureName="spActualizarCantidadesProveedores",
        parameters={
            @StoredProcedureParameter(mode=ParameterMode.IN, name = "fechaLimite", type = Date.class),
            @StoredProcedureParameter(mode=ParameterMode.OUT, name = "numProveedoresAfectados", type = Long.class)
        }
        )
public class Proveedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "clave")
    private String clave;
    @Size(max = 200)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 200)
    @Column(name = "email_contacto")
    private String emailContacto;
    @Column(name = "cantidad_productos")
    private Integer cantidadProductos;
    @Column(name = "cantidad_productos_viejos")
    private Integer cantidadProductosViejos;
//    @OneToMany(mappedBy = "proveedor")
//    private List<Articulo> articuloList;

    public Proveedor() {
    }

    public Proveedor(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmailContacto() {
        return emailContacto;
    }

    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }

    public Integer getCantidadProductos() {
        return cantidadProductos;
    }

    public void setCantidadProductos(Integer cantidadProductos) {
        this.cantidadProductos = cantidadProductos;
    }

    public Integer getCantidadProductosViejos() {
        return cantidadProductosViejos;
    }

    public void setCantidadProductosViejos(Integer cantidadProductosViejos) {
        this.cantidadProductosViejos = cantidadProductosViejos;
    }

//    public List<Articulo> getArticuloList() {
//        return articuloList;
//    }
//
//    public void setArticuloList(List<Articulo> articuloList) {
//        this.articuloList = articuloList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clave != null ? clave.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedor)) {
            return false;
        }
        Proveedor other = (Proveedor) object;
        if ((this.clave == null && other.clave != null) || (this.clave != null && !this.clave.equals(other.clave))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "lania.mrysi.compras.entidades.Proveedor[ clave=" + clave + " ]";
    }

}
