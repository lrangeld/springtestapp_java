package lania.mrysi.compras.repositorios;

import java.util.Date;
import java.util.List;
import lania.mrysi.compras.entidades.Proveedor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ljrangeld
 */
public interface RepositorioProveedores extends JpaRepository<Proveedor, String> {
    
    @Procedure(name = "Proveedor.actualizarCantidades")
    Long actualizarCantidadesArticulos(@Param("fechaLimite") Date fecha);
    
    List<Proveedor> findBynombreContaining(String value);
}
