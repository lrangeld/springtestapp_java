package lania.mrysi.compras.repositorios;

import java.util.Date;
import java.util.List;
import lania.mrysi.compras.entidades.Articulo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author ljrangeld
 */
public interface RepositorioArticulos extends JpaRepository<Articulo, Integer>{
    
    List<Articulo> findBynombreContaining(String value);
    List<Articulo> findBydescripcionStartingWith(String value);
    List<Articulo> findByfechaRegistroBetween(Date f1, Date f2);
    
    @Query("SELECT art FROM Articulo art WHERE art.proveedor.nombre LIKE :cadena")
    List<Articulo> buscarArticuloPorProveedor(@Param("cadena") String cadena);
    
    @Query("SELECT art FROM Articulo art WHERE art.proveedor.clave LIKE :cadena")
    List<Articulo> buscarArticuloPorClaveProveedor(@Param("cadena") String cadena);
    
}
