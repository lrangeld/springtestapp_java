package lania.mrysi.compras;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author jaguilar
 */
@SpringBootApplication
public class Aplicacion {
    
    public static void main(String[] args) {
        SpringApplication.run(Aplicacion.class, args);
    }
}
