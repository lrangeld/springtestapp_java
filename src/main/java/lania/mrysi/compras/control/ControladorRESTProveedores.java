package lania.mrysi.compras.control;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lania.mrysi.compras.entidades.Proveedor;
import lania.mrysi.compras.repositorios.RepositorioProveedores;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author ljrangeld
 */
@RestController
public class ControladorRESTProveedores {

    @Autowired
    RepositorioProveedores repoProveedores;


    @GetMapping("/proveedores")
    public List<Proveedor> getProveedores() {
        return repoProveedores.findAll();
    }
    
    @GetMapping("/proveedores/{clave}")
    public ResponseEntity<Proveedor> getProveedor(@PathVariable("clave") String clave) {
        Optional<Proveedor> prov = repoProveedores.findById(clave);
        if (!prov.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(prov.get());
    }

    @GetMapping(value = "/proveedores", params = {"nombre"})
    public List<Proveedor> buscarProveedoresPorNombre(
            @RequestParam("nombre") String cadena) {
        return repoProveedores.findBynombreContaining(cadena);
    }

    @PostMapping("/proveedores")
    public ResponseEntity crearProveedor(@RequestBody @Valid Proveedor prov, Errors errores) {
        try {
            if (errores.hasFieldErrors()) {
                String mensaje = errores.getFieldErrors()
                        .stream()
                        .map(fe -> fe.getField() + " " + fe.getDefaultMessage())
                        .collect(Collectors.joining(","));
                return ResponseEntity
                        .badRequest()
                        .header("ERROR", mensaje)
                        .build();
            }
            repoProveedores.save(prov);

            URI urlNuevoProveedor = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{clave}")
                    .build(prov.getClave());
            return ResponseEntity
                    .created(urlNuevoProveedor)
                    .build();
        } catch (Exception ex) {
            return ResponseEntity
                    .status(500)
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }

    @PutMapping("/proveedores/{clave}")
    public ResponseEntity actualizarProveedor(
            @PathVariable("clave") String clave,
            @RequestBody Proveedor prov
    ) {
        try {
            prov.setClave(clave);
            repoProveedores.save(prov);
            return ResponseEntity
                    .ok()
                    .build();
        } catch (Exception ex) {
            return ResponseEntity
                    .status(500)
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }
    
    @DeleteMapping("/proveedores/{clave}")
    public ResponseEntity borrarProveedor(@PathVariable("clave") String clave) {
        try {
            repoProveedores.deleteById(clave);
            return ResponseEntity
                    .ok()
                    .build();
        } catch (Exception ex) {
            return ResponseEntity
                    .status(500)
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }
}

