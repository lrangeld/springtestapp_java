package lania.mrysi.compras.control;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.validation.Valid;
import lania.mrysi.compras.entidades.Articulo;
import lania.mrysi.compras.repositorios.RepositorioArticulos;
import lania.mrysi.compras.servicios.ServicioArticulos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 *
 * @author LANIA
 */
@RestController
public class ControladorRESTArticulos {

    @Autowired
    RepositorioArticulos repoArticulos;

    @Autowired
    ServicioArticulos servArticulos;

    @GetMapping("/articulos")
    public List<Articulo> getArticulos() {
        return repoArticulos.findAll();
    }

    /* //NO considera ids no existentes
    @GetMapping("/articulos/{id}")
    public Articulo getArticulo(@PathVariable(value="id", required = true) Integer id) {
        return repoArticulos.getArticuloPorId(id);
    }*/
    @GetMapping("/articulos/{id}")
    public ResponseEntity<Articulo> getArticulo(@PathVariable("id") Integer id) {
        Optional<Articulo> art = repoArticulos.findById(id);
        if (!art.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(art.get());
    }

    @GetMapping(value = "/articulos", params = {"nombre"})
    public List<Articulo> buscarArticulosPorNombre(
            @RequestParam("nombre") String cadena) {
        return repoArticulos.findBynombreContaining(cadena);
    }

    @PostMapping("/articulos")
    public ResponseEntity crearArticulo(@RequestBody @Valid Articulo art, Errors errores) {
        try {
            if (errores.hasFieldErrors()) {
                String mensaje = errores.getFieldErrors()
                        .stream()
                        .map(fe -> fe.getField() + " " + fe.getDefaultMessage())
                        .collect(Collectors.joining(","));
                return ResponseEntity
                        .badRequest()
                        .header("ERROR", mensaje)
                        .build();
            }
            //repoArticulos.guardarArticulo(art);
            servArticulos.guardarArticulo(art);

            URI urlNuevoArticulo = ServletUriComponentsBuilder
                    .fromCurrentRequest()
                    .path("/{id}")
                    .build(art.getId());
            return ResponseEntity
                    .created(urlNuevoArticulo)
                    //.body(art);
                    .build();
        } catch (Exception ex) {
            return ResponseEntity
                    .status(500)
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }

    @PutMapping("/articulos/{id}")
    public ResponseEntity actualizarArticulo(
            @PathVariable("id") Integer id,
            @RequestBody Articulo art
    ) {
        try {
            art.setId(id);
            repoArticulos.save(art);
            return ResponseEntity
                    .ok()
                    .build();
        } catch (Exception ex) {
            return ResponseEntity
                    .status(500)
                    .header("ERROR", ex.getMessage())
                    .build();
        }
    }

//    @PatchMapping("/articulos/{id}")
//    public ResponseEntity modificarArticulo(
//            @PathVariable("id") Integer id,
//            @RequestBody Articulo art
//    ) {
//        try {
//            Articulo artOriginal = repoArticulos.getArticuloPorId(id);
//            artOriginal.setPrecio(art.getPrecio());
//            artOriginal.setDescripcion(art.getDescripcion());
//            repoArticulos.guardarArticulo(artOriginal);
//            return ResponseEntity
//                    .ok()
//                    .build();
//        } catch (Exception ex) {
//            return ResponseEntity
//                    .status(500)
//                    .header("ERROR", ex.getMessage())
//                    .build();
//        }
//    }
    
    @DeleteMapping("/articulos/{id}")
    public ResponseEntity borrarArticulo(@PathVariable("id") Integer id) {
        try {
            repoArticulos.deleteById(id);
            return ResponseEntity
                    .ok()
                    .build();
        } catch (Exception ex) {
            return ResponseEntity
                    .status(500)
                    .header("ERROR al borrar:", ex.getMessage())
                    .build();
        }
    }
    
    @GetMapping(value = "/articulos", params = {"id_proveedor"})
    public List<Articulo> buscarArticuloPorProveedor(
            @RequestParam("id_proveedor") String cadena) {
        return repoArticulos.buscarArticuloPorClaveProveedor(cadena);
    }
}
